# -*- coding: utf-8 -*-
"""blood_donation.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1MGhrvdzilPamZ2W6sFfQVlkYcSWdDNxS
"""

!pip install kaggle

from google.colab import drive
drive.mount('/content/drive')

# Commented out IPython magic to ensure Python compatibility.
# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load in

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
#Importing library for visualization
import matplotlib.pyplot as plt
import seaborn as sns
# %matplotlib inline

#Importing all the required model for model comparision
from sklearn.ensemble import RandomForestClassifier

from sklearn.linear_model import LogisticRegression

from sklearn.tree import DecisionTreeClassifier

from sklearn.neural_network import MLPClassifier

from sklearn.svm import SVC

#Importing library for splitting model into train and test and for data transformation
from sklearn.model_selection import train_test_split

from sklearn.metrics import confusion_matrix,accuracy_score,roc_auc_score

import os
os.environ['KAGGLE_CONFIG_DIR'] = '/content'

# Kaggle API credentials (replace with your own credentials)
os.environ['KAGGLE_USERNAME'] = 'pallavidonapati'
os.environ['KAGGLE_KEY'] = '8e9382616c8d266e5fa06608e0e13e17'

!kaggle datasets download -d bonastreyair/predicting-blood-analysis

# Extract the downloaded zip file
!unzip predicting-blood-analysis.zip

import pandas as pd
train = pd.read_csv("/content/blood-train.csv")
test = pd.read_csv("/content/blood-test.csv")

train.head()

train.tail()

test.tail()

#Printing the train and test size
print("Train Shape : ",train.shape)
print("Test Shape : ",test.shape)

#Counting the number of people who donated and not donated
train["Made Donation in March 2007"].value_counts()

#Storing dependent variable in Y
Y=train.iloc[:,-1]
Y.head()

#Removing Unnamed: 0 columns
old_train=train[:, 1:]
train=train.iloc[:,1:5]
test=test.iloc[:,1:5]

#Printing firsr  rows
train.head()

df=pd.merge(train,test)

df.head()

#Setting the independent variable and dependent variable
X=df.iloc[:,:]
X.head()

train.describe()

#Boxplot for Months since Last Donation
plt.figure(figsize=(20,10))
sns.boxplot(y="Months since Last Donation",data=old_train)
plt.show()

#Correlation between all variables [Checking how different variable are related]
corrmat=old_train.corr()
f, ax = plt.subplots(figsize =(9, 8))
sns.heatmap(corrmat, ax = ax, cmap ="YlGnBu", linewidths = 0.1,fmt = ".2f",annot=True)
plt.show()

#Printing all unique value for Month Since Last donation
train["Months since Last Donation"].unique()

#Creating new variable for calculating how many times a person have donated
X["Donating for"] = (X["Months since First Donation"] - X["Months since Last Donation"])

X.head()

#Dropping the unnecessary column
X.drop([ 'Total Volume Donated (c.c.)'], axis=1, inplace=True)

X.head()

#Shape of independent variable
X.shape

#Feature Scaling
from sklearn.preprocessing import StandardScaler
scale=StandardScaler()

#Fitting and transforming data
X=scale.fit_transform(X)

train=X[:576]

train.shape

test=X[576:]

Y=Y[:576]
Y.shape

#Splitting into train and test set
xtrain,xtest,ytrain,ytest=train_test_split(train,Y,test_size=0.2,random_state=42)

#Building the model
logreg = LogisticRegression(random_state=7)
#Fitting the model
logreg.fit(xtrain,ytrain)

#Predicting on the test data
pred=logreg.predict(xtest)

accuracy_score(pred,ytest)

### SVC classifier
SVMC = SVC(probability=True)
#Fitting the model
SVMC.fit(train,Y)

#Predicting on the test data
pred=SVMC.predict(xtest)

accuracy_score(pred, ytest)

#Printing the confusion matrix
confusion_matrix(pred,ytest)

#Printing the roc auc score
roc_auc_score(pred,ytest)

#Buildin the model
RFC = RandomForestClassifier()
#Fitting the model
RFC.fit(xtrain,ytrain)

pred=RFC.predict(xtest)

confusion_matrix(pred,ytest)

accuracy_score(pred, ytest)

#Printingthe roc auc score
roc_auc_score(pred,ytest)

#Building the model
model=DecisionTreeClassifier(max_leaf_nodes=4,max_features=3,max_depth=15)

model.fit(xtrain,ytrain)

#Predicting the test data
pred=model.predict(xtest)

accuracy_score(pred, ytest)

#printing the confusion matrix
confusion_matrix(pred,ytest)

#Printing roc auc score
roc_auc_score(pred,ytest)

import xgboost as xgb
from sklearn.model_selection import GridSearchCV

# Define the parameter grid to search
param_grid = {
    'max_depth': [3, 5, 7],
    'learning_rate': [0.1, 0.01, 0.001],
    'n_estimators': [20, 30, 50],
    'min_child_weight': [1, 3, 5],
    'gamma': [0, 0.1, 0.2],
    'subsample': [0.8, 0.9, 1.0],
    'colsample_bytree': [0.8, 0.9, 1.0],
    'scale_pos_weight': [1, 2, 3]
}

# Initialize the XGBoost Classifier
xgb_classifier = xgb.XGBClassifier(random_state=42)

# Initialize GridSearchCV
grid_search = GridSearchCV(xgb_classifier, param_grid, cv=5, scoring='accuracy')

# Fit GridSearchCV to the data
grid_search.fit(xtrain, ytrain)

# Get the best parameters
best_params = grid_search.best_params_

# Get the best model
best_xgb_model = grid_search.best_estimator_

# Evaluate the best model
accuracy = best_xgb_model.score(xtest, ytest)

print("Best Parameters:", best_params)
print("Accuracy of Best Model:", accuracy)

